<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'login'])->name('login');
Route::controller(LoginController::class)->group(function () {
    Route::get('/admin', 'login')->name('admin_login');
    Route::get('/login', 'login')->name('login');
    Route::post('/login', 'login_process')->name('login_process');
    Route::get('/logout', 'logout')->name('logout');
});


Route::group(['middleware' => 'auth'],function(){
    Route::get('/admin', [LoginController::class, 'admin'])->name('admin');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');



    /****************************************User Route Start Here****************************************/
    Route::get('/user', [UserController::class, 'index'])->name('user');
    Route::get('/user-create', [UserController::class, 'user_create'])->name('user_create');
    Route::post('/user-create', [UserController::class, 'user_store'])->name('user_store');
    Route::get('/user-edit/{id}', [UserController::class, 'user_edit'])->name('user_edit');
    Route::post('/user-update/{id}', [UserController::class, 'user_update'])->name('user_update');
    Route::get('/user-status/{id}', [UserController::class, 'user_status_change'])->name('user_status_change');
    Route::get('/user-remove/{id}', [UserController::class, 'user_remove'])->name('user_remove');
    Route::get('/password-reset/{id}', [UserController::class, 'password_reset'])->name('password_reset');
    Route::get('/password-change', [UserController::class, 'password_change'])->name('password_change');
    Route::post('/password-change', [UserController::class, 'password_change_process'])->name('password_change_process');
    /****************************************User Route End Here****************************************/

    /****************************************Role Route Start Here****************************************/
    Route::controller(RoleController::class)->group(function () {
        Route::get('/role', 'index')->name('role');
        Route::get('/role-create', 'role_create')->name('role_create');
        Route::post('/role-create', 'role_store')->name('role_store');
        Route::get('/role-edit/{uuid}', 'role_edit')->name('role_edit');
        Route::post('/role-update/{uuid}', 'role_update')->name('role_update');
        Route::get('/role-status/{uuid}', 'role_status_change')->name('role_status_change');
        Route::get('/role-remove/{uuid}', 'role_remove')->name('role_remove');
        Route::get('/role-assign', 'role_assign')->name('role_assign');
        Route::post('/role-assign', 'assign_role_store')->name('assign_role_store');
        Route::get('/role-assign/{uuid}', 'single_role_assign')->name('single_role_assign');
        Route::get('/remove-assign-role/{user}/{role}', 'remove_assign_role')->name('remove_assign_role');
    });
    /****************************************Role Route End Here****************************************/

    /****************************************Permission Route Start Here****************************************/
    Route::controller(PermissionController::class)->group(function () {
        Route::get('/permission', 'index')->name('permission');
        Route::get('/permission-create', 'permission_create')->name('permission_create');
        Route::post('/permission-create', 'permission_store')->name('permission_store');
        Route::get('/permission-edit/{uuid}', 'permission_edit')->name('permission_edit');
        Route::post('/permission-update/{uuid}', 'permission_update')->name('permission_update');
        Route::get('/permission-status/{uuid}', 'permission_status_change')->name('permission_status_change');
        Route::get('/permission-remove/{uuid}', 'permission_remove')->name('permission_remove');
        Route::get('/remove-assign-permission/{role}/{permission}', 'remove_assign_permission')->name('remove_assign_permission');
        Route::get('/permission-assign', 'permission_assign')->name('permission_assign');
        Route::get('/permission-assign/{uuid}', 'single_permission_assign')->name('single_permission_assign');
        Route::post('/permission-assign', 'assign_permission_store')->name('assign_permission_store');
        Route::get('/permission-assign-direct/{uuid}', 'single_permission_assign_direct')->name('single_permission_assign_direct');
        Route::post('/permission-assign-direct', 'assign_direct_permission_store')->name('assign_direct_permission_store');
        Route::get('/remove-assign-direct-permission/{user}/{permission}', 'remove_assign_direct_permission')->name('remove_assign_direct_permission');
    });
    /****************************************Permission Route End Here****************************************/
});
