<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index()
    {
        try {
            $data = Permission::paginate(20);
            return view('backend.permission.index', compact('data'));
        } catch (RecordsNotFoundException $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function permission_create()
    {
        try {
            return view('backend.permission.create');
        } catch (\Exception $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function permission_store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required|string|min:3|max:64|unique:permissions',
            ], [
                'name.required' => 'Name is required.',
                'name.min' => 'Name min length is 3.',
                'name.max' => 'Name max length is 64.',
                'name.unique' => 'Name Must Be Unique.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $data = [
                'uuid'      =>$this->uuid(),
                'name'      =>$request->name,
            ];

            Permission::create($data);
            notyf()->addSuccess('Permission Added Successfully.');
            return redirect()->route('permission');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function permission_edit($uuid)
    {
        try {
            $data = Permission::select('uuid', 'name')->where('uuid', $uuid)->first();
            return view('backend.permission.edit', compact('data'));
        } catch (RecordsNotFoundException $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function permission_update(Request $request, $uuid)
    {
        try {
            $find_existing_data = Permission::select('id')->where('uuid', $uuid)->first();
            $validator = Validator::make($request->all(),[
                'name' => 'required|string|min:3|max:64 | unique:permissions,name,'. $find_existing_data->id,
            ], [
                'name.required' => 'Name is required.',
                'name.min' => 'Name min length is 3.',
                'name.max' => 'Name max length is 64.',
                'name.unique' => 'Name Must Be Unique.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $data = [
                'name'  =>$request->name,
            ];

            Permission::where('id', $find_existing_data->id)->update($data);
            notyf()->addSuccess('Permission Update Successfully.');
            return redirect()->route('permission');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function permission_status_change($uuid)
    {
        try {
            $find_existing_data = Permission::select('id', 'uuid', 'status')->where('uuid', $uuid)->first();
            if ($find_existing_data->status === 0){
                $status = 1;
            } else{
                $status = 0;
            }

            $data = [
                'status'    =>$status
            ];

            Permission::where('id', $find_existing_data->id)->update($data);
            if ($find_existing_data->status === 0){
                notyf()->addSuccess('Permission Active Successfully.');
            } else{
                notyf()->addSuccess('Permission inactive Successfully.');
            }

            return redirect()->route('permission');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }

    }

    public function permission_remove($uuid)
    {
        try {
            $find_existing_data = Permission::where('uuid', $uuid)->first();
            $find_existing_data->delete();

            notyf()->addSuccess('Permission Remove Successfully');
            return back();
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function remove_assign_permission($role, $permission)
    {
        try {
            $role = Role::findById($role);
            $role->revokePermissionTo($permission);

            notyf()->addSuccess('Permission Remove Successfully.');
            return redirect()->route('role');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function permission_assign()
    {
        try {
            $permission = Permission::all();
            $role = Role::all();
            return view('backend.permission.assign', compact('permission', 'role'));
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function single_permission_assign($uuid)
    {
        try {
            $permission = Permission::all();
            $role = Role::where('uuid', $uuid)->first();
            return view('backend.permission.single_assign', compact('permission', 'role'));
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function assign_permission_store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'role_id' => 'required',
            ], [
                'role_id.required' => 'Name is required.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
            $role = Role::findById($request->role_id);
            foreach ($request->permissions_id as $permission_id){
                $role->givePermissionTo($permission_id);
            }

            notyf()->addSuccess('Permission Assign Successfully.');
            return redirect()->route('role');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function single_permission_assign_direct($uuid)
    {
        try {
            $permission = Permission::all();
            $user = User::where('uuid', $uuid)->first();
            return view('backend.permission.direct_assign', compact('user', 'permission'));
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function assign_direct_permission_store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'user_id' => 'required',
            ], [
                'user_id.required' => 'User is required.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
            $user = User::find($request->user_id);

            foreach ($request->permissions_id as $permission_id){
                $user->givePermissionTo($permission_id);
            }

            notyf()->addSuccess('Permission Assign Successfully.');
            return redirect()->route('user');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function remove_assign_direct_permission($user, $permission)
    {
        try {
            $user = User::find($user);
            $user->revokePermissionTo($permission);

            notyf()->addSuccess('Permission Remove Successfully.');
            return redirect()->route('user');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }
}
