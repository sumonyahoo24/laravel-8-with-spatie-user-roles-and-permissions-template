<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Database\QueryException;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        try {
            $data = User::where('id', '!=', 1)->orderBy("id", "DESC")->paginate(20);
            return view('backend.user.index', compact('data'));
        } catch (RecordsNotFoundException $exception) {
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }
    }

    public function user_create()
    {
        try {
            return view('backend.user.create');
        } catch (\Exception $exception) {
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }
    }

    public function user_store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required|string|min:3|max:64',
                'phone'  => 'required | min:10 | max:32 | unique:users',
                'email'  => 'required | email | unique:users',
            ], [
                'name.required' => 'Name is required.',
                'name.min' => 'Name min length is 3.',
                'name.max' => 'Name max length is 64.',
                'phone.required' => 'Phone is required.',
                'phone.unique' => 'Phone is Unique.',
                'phone.min' => 'Phone min length is 3.',
                'phone.max' => 'Phone max length is 32.',
                'email.required' => 'Email is required.',
                'email.email' => 'Email is not valid.',
                'email.unique' => 'Phone is Unique.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $request_data = [
                'uuid'      =>$this->uuid(),
                'name'      =>$request->name,
                'email'     =>$request->email,
                'phone'     =>$request->phone,
                'password'  =>Hash::make($request->phone),
            ];

            User::create($request_data);

            notyf()->addSuccess('User Added Successfully.');
            return redirect()->route('user');
        } catch (QueryException $exception) {
            notyf()->addWarning('Something went wrong. Please try again.');
            return back();
        } catch (\Exception $exception){
            notyf()->addWarning('Something went wrong.');
            return back();
        }
    }

    public function user_edit($uuid)
    {
        try {
            $data = User::where('uuid', $uuid)->first();
            return view('backend.user.edit', compact('data'));
        } catch (RecordsNotFoundException $exception) {
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }
    }

    public function user_update(Request $request, $uuid)
    {
        try {
            $find_existing_data = User::where('uuid', $uuid)->first();
            if ($find_existing_data) {
                $validator = Validator::make($request->all(),[
                    'name' => 'required|string|min:3|max:64',
                    'phone'  => 'required | min:10 | max:32 | unique:users,phone,'. $find_existing_data->id,
                    'email'  => 'required | email | unique:users,email,'. $find_existing_data->id,
                ], [
                    'name.required' => 'Name is required.',
                    'name.min' => 'Name min length is 3.',
                    'name.max' => 'Name max length is 64.',
                    'phone.required' => 'Phone is required.',
                    'email.required' => 'Email is required.',
                    'email.email' => 'Email is not valid.',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator);
                }

                $update_data = [
                    'name'      =>$request->name,
                    'email'      =>$request->email,
                    'phone'      =>$request->phone,
                ];

                $find_existing_data->update($update_data);
                notyf()->addSuccess('User Update Successfully.');
                return redirect()->route('user');
            } else{
                notyf()->addError('Data Not Found.');
                return back();
            }
        } catch (QueryException $exception) {
            notyf()->addWarning('Something went wrong. Please try again.');
            return back();
        } catch (\Exception $exception){
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }

    }

    public function user_status_change($uuid)
    {
        try {
            $find_existing_data = User::where('uuid', $uuid)->first();
            if ($find_existing_data) {
                if ($find_existing_data->status === 0){
                    $status = 1;
                } else{
                    $status = 0;
                }

                $update_data = [
                    'status'      =>$status
                ];

                $find_existing_data->update($update_data);
                if ($find_existing_data->status === 0){
                    notyf()->addSuccess('User Active Successfully.');
                } else{
                    notyf()->addSuccess('User inactive Successfully.');
                }

                return redirect()->route('user');
            } else{
                notyf()->addError('Data Not Found.');
                return back();
            }
        } catch (\Exception $exception){
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }

    }

    public function user_remove($uuid)
    {
        try {
            $find_existing_data = User::where('uuid', $uuid)->first();
            $find_existing_data->delete();

            notyf()->addSuccess('User Remove Successfully.');
            return back();
        } catch (\Exception $exception){
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }
    }

    public function password_reset($uuid)
    {
        try {
            $find_existing_data = User::where('uuid', $uuid)->first();
            if ($find_existing_data->phone){
                $update_data = [
                    'password'      =>Hash::make($find_existing_data->phone),
                ];
                $find_existing_data->update($update_data);
                notyf()->addSuccess('User Password Reset Successfully.');
                return redirect()->route('user');
            }
        } catch (RecordsNotFoundException $exception) {
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }
    }

    public function password_change()
    {
        try {
            return view('backend.user.password_change');
        } catch (RecordsNotFoundException $exception) {
            notyf()->addWarning('Something went wrong.');
            return back()->withErrors($exception->getMessage());
        }
    }

    public function password_change_process(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'old_password'  => 'required',
                'new_password'  => 'required | min:4 | max:32 ',
                'new_confirm_password'  => 'required | min:4 | max:32 ',
            ], [
                'old_password.required' => 'Old Password is required.',
                'new_password.required' => 'New Password is required.',
                'new_password.min' => 'New Password Minimum 4.',
                'new_password.max' => 'New Password Maximum 4.',
                'new_confirm_password.required' => 'New Confirm Password is required.',
                'new_confirm_password.min' => 'New Confirm Password Minimum 4.',
                'new_confirm_password.max' => 'New Confirm Password Maximum 4.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
            $auth_user = User::where('id', Auth::id())->first();

            $check = Hash::check($request->old_password, $auth_user->password);
            if ($check == true) {

                $request_data = [
                    'password'      =>Hash::make($request->new_password ?? $auth_user->phone),
                ];

                $auth_user->update($request_data);
                Auth::logout();
                notyf()->addSuccess('Password Change Successfully.');
                return redirect()->route('login');
            } else{
                notyf()->addWarning('Old password did not match. Please try again.');
                return back();
            }
        } catch (QueryException $exception) {
            notyf()->addWarning('Something went wrong. Please try again.');
            return back();
        } catch (\Exception $exception){
            notyf()->addWarning('Something went wrong.');
            return back();
        }
    }
}
