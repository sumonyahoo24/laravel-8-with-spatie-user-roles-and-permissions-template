<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function admin()
    {
        if (Auth::check()){
            return redirect()->route('dashboard');
        } else{
            return view('backend.login.index');
        }
    }
    public function login()
    {
        if (Auth::check()){
            return redirect()->route('dashboard');
        } else{
            return view('backend.login.index');
        }
    }
    public function login_process(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'phone'  => 'required',
                'password'  => 'required',
            ], [
                'phone.required' => 'Email is required.',
                'password.required' => 'Password is required.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $check_user_status = User::where('phone', $request->phone)->first();

            if ($check_user_status){
                if ($check_user_status->status == 1){
                    $credentials = $request->only('phone', 'password');
                    if (Auth::attempt($credentials)) {
                        notyf()->addInfo('Welcome Back');
                        return redirect()->intended('dashboard');
                    }
                    notyf()->addError('Wrong Credentials. Please Try Again');
                    return redirect("login");
                } else{
                    notyf()->addError('Please contact with Admin');
                    return redirect("login");
                }
            } else{
                notyf()->addError('Something went wrong.');
                return redirect("login");
            }


        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function logout()
    {
        Auth::logout();
        notyf()->addSuccess('Logout Successfully. Thanks');
        return redirect()->route('admin');
    }
}
