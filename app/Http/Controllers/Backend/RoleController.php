<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        try {
            $data = Role::with('permissions')->paginate(20);
            return view('backend.role.index', compact('data'));
        } catch (RecordsNotFoundException $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function role_create()
    {
        try {
            return view('backend.role.create');
        } catch (\Exception $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function role_store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required|string|min:3|max:64|unique:roles',
            ], [
                'name.required' => 'Name is required.',
                'name.min' => 'Name min length is 3.',
                'name.max' => 'Name max length is 64.',
                'name.unique' => 'Name Must Be Unique.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $data = [
                'uuid'      =>$this->uuid(),
                'name'      =>$request->name,
            ];

            $role = Role::create($data);
            notyf()->addSuccess('Role Added Successfully.');
            return redirect()->route('role');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function role_edit($uuid)
    {
        try {
            $data = Role::select('uuid', 'name')->where('uuid', $uuid)->first();
            return view('backend.role.edit', compact('data'));
        } catch (RecordsNotFoundException $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function role_update(Request $request, $uuid)
    {
        try {
            $find_existing_data = Role::select('id')->where('uuid', $uuid)->first();
            $validator = Validator::make($request->all(),[
                'name' => 'required|string|min:3|max:64 | unique:roles,name,'. $find_existing_data->id,
            ], [
                'name.required' => 'Name is required.',
                'name.min' => 'Name min length is 3.',
                'name.max' => 'Name max length is 64.',
                'name.unique' => 'Name Must Be Unique.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $data = [
                'name'  =>$request->name,
            ];

            Role::where('id', $find_existing_data->id)->update($data);
            notyf()->addSuccess('Role Update Successfully.');
            return redirect()->route('role');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function role_status_change($uuid)
    {
        try {
            $find_existing_data = Role::select('id', 'uuid', 'status')->where('uuid', $uuid)->first();
            if ($find_existing_data->status === 0){
                $status = 1;
            } else{
                $status = 0;
            }

            $data = [
                'status'    =>$status
            ];

            Role::where('id', $find_existing_data->id)->update($data);
            if ($find_existing_data->status === 0){
                notyf()->addSuccess('Role Active Successfully.');
            } else{
                notyf()->addSuccess('Role inactive Successfully.');
            }

            return redirect()->route('role');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }

    }

    public function role_remove($uuid)
    {
        try {
            $find_existing_data = Role::where('uuid', $uuid)->first();
            $find_existing_data->delete();

            notyf()->addSuccess('Role Remove Successfully');
            return back();
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function role_assign()
    {
        try {
            $user = User::whereStatus(1)->get();
            $role = Role::all();
            return view('backend.role.assign', compact('user', 'role'));
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function assign_role_store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'user_id' => 'required',
            ], [
                'user_id.required' => 'User is required.',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }
            $user = User::find($request->user_id);
            foreach ($request->roles_id as $role_id){
                $user->assignRole($role_id);
            }

            notyf()->addSuccess('Role Assign Successfully.');
            return redirect()->route('user');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function single_role_assign($uuid)
    {
        try {
            $role = Role::all();
            $user = User::where('uuid', $uuid)->first();
            return view('backend.role.single_assign', compact('user', 'role'));
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }

    public function remove_assign_role($user, $role)
    {
        try {
            $user = User::find($user);
            $user->removeRole($role);

            notyf()->addSuccess('Role Remove Successfully.');
            return redirect()->route('user');
        } catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }
}
