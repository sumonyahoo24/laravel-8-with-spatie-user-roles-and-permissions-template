@extends('backend.master')
@section('title', 'Role Assign')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h3 class="card-title">Role Assign</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="{{ route('role') }}" class="btn btn-sm btn-success px-3">List</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('assign_role_store') }}" method="POST">
                        @CSRF
                        <input type="hidden" name="user_id" value="{{ $user->id }}" />
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="roles_id" class="form-label">Role</label>
                                    <select id="roles_id" class="multiple-select" name="roles_id[]" data-placeholder="Choose anything" multiple="multiple"
                                            required>
                                        <option value="">Select Role</option>
                                        @forelse($role as $role_data)
                                            <option value="{{ $role_data->id ?? '' }}">{{ $role_data->name ?? '' }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 text-left">
                                <button class="btn btn-primary" type="submit">Assign</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('.multiple-select').select2({
            theme: 'bootstrap4',
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    </script>
@endpush
