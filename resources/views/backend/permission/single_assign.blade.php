@extends('backend.master')
@section('title', 'Permission Assign')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h3 class="card-title">Permission Assign</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="{{ route('permission') }}" class="btn btn-sm btn-success px-3">List</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('assign_permission_store') }}" method="POST">
                        @CSRF
                        <input type="hidden" name="role_id" value="{{ $role->id }}" />
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="permissions_id" class="form-label">Permission</label>
                                    <select id="permissions_id" class="multiple-select" name="permissions_id[]" data-placeholder="Choose anything" multiple="multiple"
                                            required>
                                        <option value="">Select Permission</option>
                                        @forelse($permission as $permission_data)
                                            <option value="{{ $permission_data->id ?? '' }}">{{ $permission_data->name ?? '' }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 text-left">
                                <button class="btn btn-primary" type="submit">Assign</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('.multiple-select').select2({
            theme: 'bootstrap4',
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    </script>
@endpush
