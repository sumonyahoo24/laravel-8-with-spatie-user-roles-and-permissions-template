@extends('backend.master')
@section('title', 'Permission Edit')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h3 class="card-title">Permission Edit</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="{{ route('permission') }}" class="btn btn-sm btn-success px-3">List</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('permission_update', $data->uuid) }}" method="POST">
                        @CSRF
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control form-control-sm" value="{{ $data->name ?? '' }}" />
                                </div>
                            </div>
                            <div class="col-md-12 text-left">
                                <button class="btn btn-primary" type="submit">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
