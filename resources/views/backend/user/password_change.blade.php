@extends('backend.master')
@section('title', 'Password Change')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h3 class="card-title">Password Change</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="{{ route('dashboard') }}" class="btn btn-sm btn-success px-3">Dashboard</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('password_change_process') }}" method="POST">
                        @CSRF
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="old_password">Old Password</label>
                                    <input type="password" name="old_password" id="old_password" class="form-control form-control-sm" placeholder="Old Password" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="new_password">New Password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control form-control-sm" placeholder="New Password" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="new_confirm_password">New Confirm Password</label>
                                    <input type="password" name="new_confirm_password" id="new_confirm_password" class="form-control form-control-sm" placeholder="New Confirm Password" />
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary" type="submit">Password Change</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
