@extends('backend.master')
@section('title', 'User Update')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h3 class="card-title">User Update</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="{{ route('user') }}" class="btn btn-sm btn-success px-3">List</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('user_update', $data->uuid) }}" method="POST">
                        @CSRF
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="name">Full Name</label>
                                    <input type="text" name="name" id="name" class="form-control form-control-sm" value="{{ $data->name ?? '' }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="phone">Phone</label>
                                    <input type="text" name="phone" id="phone" class="form-control form-control-sm" value="{{ $data->phone ?? '' }}" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-form-label" for="email">Email</label>
                                    <input type="text" name="email" id="email" class="form-control form-control-sm" value="{{ $data->email ?? '' }}" />
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
