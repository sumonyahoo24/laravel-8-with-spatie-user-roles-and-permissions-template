@extends('backend.master')
@section('title', 'User')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h3 class="card-title">User List</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                            <a href="{{ route('user_create') }}" class="btn btn-sm btn-success px-3">Add</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0 font-13">
                            <thead class="table-secondary">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Permission</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($data as $key=>$item)
                                <tr class="border_bottom">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name ?? '' }}</td>
                                    <td>{{ $item->phone ?? '' }}</td>
                                    <td>{{ $item->email ?? '' }}</td>
                                    <td>
                                        @forelse($item->roles as $role)
                                            <span class="badge bg-primary">
                                                {{ $role->name ?? '' }}
                                                <a href="{{ route('remove_assign_role', ['user'=>$item->id, 'role'=>$role->id] )}}"
                                                   class="badge bg-dark">x</a>
                                            </span>
                                        @empty
                                            <p>No Role</p>
                                        @endforelse
                                    </td>
                                    <td>
                                        @forelse($item->permissions as $permission)
                                            <span class="badge bg-primary">
                                            {{ $permission->name ?? '' }}
                                            <a href="{{ route('remove_assign_direct_permission', ['user'=>$item->id, 'permission'=>$permission->id] )}}"
                                               class="badge bg-dark">x</a>
                                        </span>
                                        @empty
                                            <p>No Permission</p>
                                        @endforelse
                                    </td>
                                    <td>
                                        <a href="{{ route('user_status_change', $item->uuid) }}" class="btn btn-sm {{ $item->status ===1 ? 'btn-success' : 'btn-danger' }}">
                                            @if($item->status === 1)
                                                Active
                                            @else
                                                Inactive
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('single_role_assign', $item->uuid) }}"
                                           class="btn btn-sm btn-primary p-1" title="Assign Role">
                                            <i class="fas fa-tasks"></i>
                                        </a>
                                        <a href="{{ route('single_permission_assign_direct', $item->uuid) }}"
                                           class="btn btn-sm btn-info p-1" title="Assign Permission">
                                            <i class="fas fa-tasks"></i>
                                        </a>
{{--                                        @if($auth_user->hasDirectPermission('Update'))--}}
                                            <a href="{{ route('user_edit', $item->uuid) }}"
                                               class="btn btn-sm btn-warning p-1" title="Edit"><i class="fas fa-edit"></i></a>
{{--                                        @endif--}}
                                        @if(auth()->user()->id == 1)
                                            <a href="{{ route('user_remove', $item->uuid) }}"
                                               class="btn btn-sm btn-danger p-1" title="Remove"><span><i
                                                        class="fas fa-trash"></i></span></a>
                                            <a href="{{ route('password_reset', $item->uuid) }}"
                                               class="btn btn-sm btn-success p-1" title="Password Reset"><span><i
                                                        class="fas fa-lock-open"></i></span></a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                            @endforelse

                            </tbody>
                        </table>
{{--                        <span class="float-end p-0">{{ $data->links() }}</span>--}}
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
