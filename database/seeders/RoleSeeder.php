<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'uuid'  => 'a7bab8b4-e13b-45a5-90e7-dcff61858606',
                'name'  => 'master_admin',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'uuid'  => 'a7bab8b4-e13b-45a5-90e7-dcff61858607',
                'name'  => 'super_admin',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'uuid'  => 'ccd096c2-0c12-4977-8293-635d458a0c62',
                'name'  => 'admin',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'uuid'  => '907ee1f0-f4ec-4e03-a42b-be801e80343f',
                'name'  => 'user',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];
        Role::insert($data);

        $user = User::find(1);
        $roles = Role::get();
        foreach ($roles as $single_role){
            $user->assignRole($single_role->id);
        }

    }
}
