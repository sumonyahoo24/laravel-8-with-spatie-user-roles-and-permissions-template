<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'uuid'  => '4074338d-fb26-4d15-9fe5-213eb04fd83d',
                'name'  => 'Create',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'uuid'  => '362de4f2-dfc0-4722-adc3-0b88a7289a03',
                'name'  => 'View',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'uuid'  => '888671c3-2b2e-47e4-bd67-418695a9b6f9',
                'name'  => 'Update',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'uuid'  => '0cb014ae-bbe6-4ac0-ba84-837fc86a8eb3',
                'name'  => 'Delete',
                'guard_name'  => 'web',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];
        Permission::insert($data);
    }
}
